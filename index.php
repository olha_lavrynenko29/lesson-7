<?php
session_start();
if (!empty($_SESSION['currencys'])) {
    $from_session = $_SESSION['currencys'];
}

$currency_all = [
    'uah' => [
        'name' => 'Гривна',
        'course' => 1,
    ],
    'usd' => [
        'name' => 'Доллар',
        'course' => 27.1,
    ],
    'euro' => [
        'name' => 'Евро',
        'course' => 30.2,
    ],
];

$products = [
    [
        'title' => 'MacBook Pro',
        'price_val' => 55645
    ],
    [
        'title' => 'iPhone 11',
        'price_val' => 19932
    ],
    [
        'title' => 'Lenovo Tab 2',
        'price_val' => 2500
    ],
    [
        'title' => 'iPhone 7',
        'price_val' => 13000
    ],
    [
        'title' => 'AirPods 2',
        'price_val' => 5490
    ],
    [
        'title' => 'AirPods Pro',
        'price_val' => 7432
    ],
    [
        'title' => 'LG Smart TV',
        'price_val' => 15000
    ],
    [
        'title' => 'LG Action Camera',
        'price_val' => 12300
    ],
    [
        'title' => 'Meizu Power Bank',
        'price_val' => 700
    ],
    [
        'title' => 'Acer Notebook',
        'price_val' => 7389
    ],
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lesson 7</title>
    <meta name="lesson 7" content="Sessions">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h2>Выбор валютыj</h2>
        <div class="container">
            <form action="./convert_process.php" method="POST">
                <select class="form-select" name="selector">
                    <option selected>Выберете валюту...</option>
                    <option <?php if ($from_session['currency'] === $currency_all['uah']['name']) : ?>selected<?php endif; ?>>Гривна</option>
                    <option <?php if ($from_session['currency'] === $currency_all['usd']['name']) : ?>selected<?php endif; ?>>Доллар</option>
                    <option <?php if ($from_session['currency'] === $currency_all['euro']['name']) : ?>selected<?php endif; ?>>Евро</option>
                </select>
        </div>
        <div class="container">
            <br>
            <button class="btn btn-dark">Отправить</button>
            <br>
        </div>
        </form>
    </div>
    <div class="container">
        <h2>Перечень товаров</h2>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Наименование товара</th>
                    <th scope="col">Цена</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product) : ?>
                    <tr>
                        <td><?= $product['title'] ?></td>
                        <?php if ($from_session['currency'] === $currency_all['uah']['name']) : ?>
                            <td><?= round($product['price_val'] / $currency_all['uah']['course'], 2) ?></td>
                        <?php elseif ($from_session['currency'] === $currency_all['usd']['name']) : ?>
                            <td><?= round($product['price_val'] / $currency_all['usd']['course'], 2) ?></td>
                        <?php elseif ($from_session['currency'] === $currency_all['euro']['name']) : ?>
                            <td><?= round($product['price_val'] / $currency_all['euro']['course'], 2) ?></td>
                        <?php else : ?>
                            <td><?= round($product['price_val'] / $currency_all['uah']['course'], 2) ?></td>
                        <?php endif; ?>
                    <?php endforeach ?>
                    </tr>
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>

</html>